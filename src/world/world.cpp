#include "world/world.hpp"
#include "data_structs/type_map.hpp"

namespace eks {

World::World(TypeMap resources): m_resources{std::move(resources)} {}

}