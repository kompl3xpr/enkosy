#include "app.hpp"

#include "plugin.hpp"
#include "resource/state.hpp"
#include "system/system_set.hpp"
#include "world/world.hpp"

namespace eks {

AppBuilder::AppBuilder() {}

std::unique_ptr<App> AppBuilder::build() {
    return std::unique_ptr<App>(new App(World(std::move(m_resources))));
}

App::App(World world) : m_world{std::move(world)} {}

void App::run() {}

}  // namespace eks