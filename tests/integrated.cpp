#include <gtest/gtest.h>

#include <eks.hpp>
#include <iostream>

enum class MyAppState {
    begin,
    end,
};

class Transform {
   public:
    Transform* m_parent;
};
class Velocity {};

struct GlobalCounter {
    int m_value;
};

void funcptr_system(eks::Query<Transform, Velocity> query,
                    eks::Res<GlobalCounter> counter) {
    counter.get().m_value += 1;

    for (auto [transform, velocity] : query) {
        if (transform.m_parent == nullptr) {
            // some logic here...
        } else {
            // some logic here...
        }
    }
}

class MyPlugin : public eks::IPlugin {
    int m_counterInit;

   public:
    MyPlugin(int counterInit) : m_counterInit{counterInit} {}

    void apply(eks::AppBuilder& builder) override {
        builder.registerComponent<Transform>()
            .registerComponent<Velocity>()
            .addResource(GlobalCounter(m_counterInit))
            .addSystem(eks::SystemSet<bool>());
    }
};

TEST(IntegratedTest, Default) {
    using namespace eks;
    EXPECT_STREQ("0.1.0", eks::getEksVersion());

    auto systemSet = SystemSet<MyAppState>();
    systemSet.addSystem(MyAppState::end, Activity::on_enter, funcptr_system)
        .addSystem(MyAppState::begin, Activity::on_update,
                   [](Res<StateOf<MyAppState>> state) {
                       std::cout << "Hello, world!\n";
                       state.get().value() = MyAppState::end;
                   });

    AppBuilder()
        .addPlugin(MyPlugin(123))
        .addSystem(std::move(systemSet))
        .initState(MyAppState::begin)
        .run();
}
