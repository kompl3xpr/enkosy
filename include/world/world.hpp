#pragma once

#include "data_structs/type_map.hpp"

namespace eks {
class World {
   public:
    TypeMap m_resources;

    World(TypeMap resources);
};
}  // namespace eks