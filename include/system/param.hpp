#pragma once

#include <tuple>

#include "world/world.hpp"

namespace eks {

template <typename T>
class Res {
    T& m_resource;

    Res(T& resource) : m_resource{resource} {}

   public:
    static Res<T> fromWorld(World& world) {
        return Res(*world.m_resources.get<T>());
    }

    T& get() { return m_resource; }
};

template <typename... Component>
class QueryIter {
   public:
    QueryIter& operator++() { return *this; }

    bool operator!=(const QueryIter<Component...> other) { return true; }

    std::tuple<Component&...> operator*() const { return this->operator*(); }
};

template <typename... Component>
class Query {
   public:
    static Query<Component...> fromWorld(World& world) {
        return Query<Component...>::fromWorld(world);
    }

    QueryIter<Component...> begin() { return QueryIter<Component...>(); }

    QueryIter<Component...> end() { return QueryIter<Component...>(); }
};
}  // namespace eks