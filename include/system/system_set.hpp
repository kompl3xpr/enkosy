#pragma once

#include <concepts>
#include <functional>
#include <tuple>
#include <vector>

#include "resource/state.hpp"
#include "system/system.hpp"

namespace eks {

template <typename T>
concept IntoSystem = std::is_convertible_v<T, System>;

template <State T>
class SystemSet {
    std::vector<std::tuple<T, Activity, System>> m_inner;

   public:
    SystemSet() {}

    template <IntoSystem S>
    SystemSet& addSystem(T state, Activity activity, S system) {
        m_inner.push_back({state, activity, system});
        return *this;
    }

    SystemSet(const SystemSet& a) = delete;
    auto& operator=(const SystemSet& a) = delete;

    SystemSet(SystemSet&& other) { m_inner = std::move(other.m_inner); }
    auto& operator=(SystemSet&& other) {
        m_inner = std::move(other.m_inner);
        return *this;
    }
};
}  // namespace eks