#pragma once

#include <functional>
#include <iostream>

namespace eks {

class World;

template <typename T, typename U>
concept NotSameAs = !std::is_same_v<T, U>;

template <typename T>
concept Lambda = requires(T t) {
    { std::function(t) } -> NotSameAs<T>;
};

template <typename T>
concept FromWorld = requires(T t, World& w) {
    { T::fromWorld(w) } -> std::same_as<T>;
};

class System {
    std::function<void(World&)> m_inner;

   public:
    template <FromWorld... Params>
    System(void (*f)(Params...)) {
        m_inner = [=](World& world) {
            auto tup = std::make_tuple(Params::fromWorld(world)...);
            std::apply(f, tup);
        };
    }

    template <FromWorld... Params>
    System(std::function<void(Params...)> f) {
        m_inner = [=](World& world) {
            auto tup = std::make_tuple(Params::fromWorld(world)...);
            std::apply(f, tup);
        };
    }

    template <Lambda L>
    System(L lambda) : System(std::function(lambda)) {}

    void apply(World& world) { (m_inner)(world); }
};

}  // namespace eks