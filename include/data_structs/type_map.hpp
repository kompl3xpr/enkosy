#pragma once

#include <any>
#include <iostream>
#include <map>
#include <typeinfo>

namespace eks {

class TypeMap {
    std::map<size_t, std::any> m_inner;

   public:
    TypeMap() = default;

    template <typename T>
    void insert(T value) {
        m_inner.emplace(typeid(T).hash_code(), std::move(value));
    }

    template <typename T>
    void erase() {
        m_inner.erase(typeid(T).hash_code());
    }

    template <typename T>
    T* get() {
        auto it = m_inner.find(typeid(T).hash_code());
        if (it != m_inner.end()) {
            return &std::any_cast<T&>(it->second);
        } else {
            return nullptr;
        }
    }
};

}  // namespace eks