#pragma once

#include "app.hpp"
#include "plugin.hpp"
#include "system/param.hpp"
#include "system/system_set.hpp"

namespace eks {

constexpr const char* getEksVersion() { return "0.1.0"; }

}  // namespace eks