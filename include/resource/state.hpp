#pragma once

#include <concepts>
#include <stack>

namespace eks {
template <typename T>
concept State = std::is_trivial_v<T>;

enum class Activity {
    on_enter = 0b00001,
    on_update = 0b00010,
    on_pause = 0b00100,
    on_resume = 0b01000,
    on_exit = 0b10000,
};

template <State T>
struct StateOf {
    T m_value;

    T& value() { return m_value; }
};
}  // namespace eks