#pragma once

#include <concepts>

template <typename T>
concept Component =
    // requires(T t) {
    //     { T::getName() } -> std::same_as<const char*>;
    // } &&
    !std::is_reference_v<T> && !std::is_pointer_v<T>;