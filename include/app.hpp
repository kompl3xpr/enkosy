#pragma once

#include <memory>

#include "component/component.hpp"
#include "data_structs/type_map.hpp"
#include "resource/state.hpp"
#include "system/system_set.hpp"
#include "world/world.hpp"

namespace eks {

class IPlugin;
class AppBuilder;
class App;

class App {
    World m_world;

    friend AppBuilder;
    App(World world);

   public:
    void run();

    App(const App& a) = delete;
    auto& operator=(const App& a) = delete;
    App(App&& a) = delete;
    auto& operator=(App&& a) = delete;
};

class AppBuilder {
    TypeMap m_resources{};

   public:
    AppBuilder();

    template <State T>
    AppBuilder& addSystem(SystemSet<T> systemSet) {
        return *this;
    }

    template <typename T>
    AppBuilder& addResource(T resource) {
        m_resources.insert(std::move(resource));
        return *this;
    }

    template <State T>
    AppBuilder& initState(T state) {
        m_resources.insert(StateOf(std::move(state)));
        return *this;
    }

    template <Component T>
    AppBuilder& registerComponent() {
        return *this;
    }

    template <typename T>
        requires std::is_base_of_v<IPlugin, T>
    AppBuilder& addPlugin(T plugin) {
        plugin.apply(*this);
        return *this;
    }

    std::unique_ptr<App> build();
    void run() { this->build()->run(); }

    AppBuilder(const AppBuilder& a) = delete;
    auto& operator=(const AppBuilder& a) = delete;
    AppBuilder(AppBuilder&& a) = delete;
    auto& operator=(AppBuilder&& a) = delete;
};
}  // namespace eks