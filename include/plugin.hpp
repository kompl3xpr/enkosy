#pragma once

namespace eks {

class AppBuilder;

class IPlugin {
   public:
    virtual void apply(AppBuilder& builder) = 0;
};

}